// Mock database
let posts = [];

// Post ID
let count = 1;

// Add post 
// This function is called when the form for adding a post is submitted.
// It retrieves the title and body values entered in the form, creates a new post object with a unique ID, and adds it to the mock database.
document.querySelector("#form-add-post").addEventListener("submit", (e) => {
// Prevent the form from submitting and refreshing the page
    e.preventDefault();

// Retrieve the title and body values entered in the form
    const title = document.querySelector("#txt-title").value;
    const body = document.querySelector("#txt-body").value;

// Create a new post object with a unique ID
    const newPost = {
id: count, // Assign a unique ID based on the current count value
title: title,
body: body,
};

// Increment the count for the next post
count++;

// Add the new post to the mock database
posts.push(newPost);

// Call the showPosts function to display the updated posts
showPosts(posts);

// Show a success message
alert("Successfully added!");
});

// Edit post
// This function is called when the Edit button is clicked for a specific post.
// It retrieves the current title and body of the post and populates the corresponding input fields in the edit form.
const editPost = (id) => {

    // Retrieve the current title and body of the post
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    // Populate the input fields in the edit form with the current post values
    document.querySelector("#txt-edit-id").value = id;
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
};

// Update post
// This function is called when the Update button is clicked in the edit form.
// It retrieves the updated title and body values from the input fields and updates the corresponding post in the mock database.
document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
    e.preventDefault();

    // Loop through every post in the "posts" array
    for (let i = 0; i < posts.length; i++) {

    // Find the post with the matching id
        if (posts[i].id == document.querySelector("#txt-edit-id").value) {

            // Update the title and body of the post with the new values from the input fields
            posts[i].title = document.querySelector("#txt-edit-title").value;
            posts[i].body = document.querySelector("#txt-edit-body").value;

            // Call the showPosts function to display the updated posts
            showPosts(posts);
            alert("Successfully updated!");
            break;
        }
    }
};

// ACTIVITY
// Delete post
// This function is called when the Delete button is clicked for a specific post.
// It finds the index of the post in the mock database based on its ID and removes it from the array.
const deletePost = (id) => {

    // Find the index of the post with the specified ID in the "posts" array
    const index = posts.findIndex((post) => post.id === id);

    if (index !== -1) {

        // Remove the post from the "posts" array using splice
        posts.splice(index, 1);

        // Call the showPosts function to display the updated posts
        showPosts(posts);

        // Show a success message
        alert("Successfully deleted!");
    }
};