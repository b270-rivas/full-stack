import React from 'react';
import LoginComponent from '../components/LoginComponent';
import { Navigate } from 'react-router-dom';

export default function Login() {
    return (
        <div>
            <LoginComponent />
        </div>
    );
}
