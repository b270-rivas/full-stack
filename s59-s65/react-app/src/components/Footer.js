import React from "react";

const Footer = () => (
  <footer className="page-footer">
    <div className="container-fluid text-center text-md-left">

    <section className="social-media d-flex justify-content-center justify-content-lg-between p-4 border-bottom">
      <div className="ml-lg-3 pl-lg-5 d-none d-lg-block">
        <span>Get connected with us on social networks:</span>
      </div>

      <div className="mr-lg-5">
        <a href="" className="icons mx-4 text-reset">
          <i className="fab fa-facebook-f"></i>
        </a>
        <a href="" className="icons mx-4 text-reset">
          <i className="fab fa-twitter"></i>
        </a>
        <a href="" className="icons mx-4 text-reset">
          <i className="fab fa-google"></i>
        </a>
        <a href="" className="icons mx-4 text-reset">
          <i className="fab fa-instagram"></i>
        </a>
        <a href="" className="icons mx-4 text-reset">
          <i className="fab fa-linkedin"></i>
        </a>
        <a href="" className="icons mx-4 text-reset">
          <i className="fab fa-github"></i>
        </a>
      </div>
    </section>

      <div className="row justify-content-center">
        <div className="col-md-4 mb-md-0 my-3 pl-lg-5">
          <img
            src={process.env.PUBLIC_URL + "/images/footer-logo.png"}
            alt="Logo"
            className="img-fluid footer-logo py-3"
          />
          <ul className="footer-list list-unstyled pl-2 pl-lg-4">
            <li>
              <strong>Address:</strong> 123 Main Street<br />
              Anytown, PH 12345
            </li>
            <li>
            </li>
            <li>
              <strong>Phone:</strong> 123-456-7890
            </li>
            <li>
              <strong>Email:</strong> info@mysite.com
            </li>
          </ul>
        </div>

        <hr className="clearfix w-100 d-md-none pb-0" />

        <div className="col-md-4 mb-md-0 my-3 pt-md-4 pl-lg-5 text-md-center">
          <h5 className="footer-h5 text-uppercase py-md-3">OPEN HOURS</h5>
          <ul className="footer-list list-unstyled">
            <li>
              <strong>Mon - Fri:</strong> 7am - 10pm
            </li>
            <li>
              <strong>Saturday:</strong> 8am - 10pm
            </li>
            <li>
              <strong>Sunday:</strong> 8am - 11pm
            </li>
          </ul>
        </div>

        <hr className="clearfix w-100 d-md-none pb-0" />

        <div className="col-md-4 mt-md-0 my-3 pt-md-4 pl-lg-5 text-center">
          <h5 className="footer-h5 text-uppercase py-md-3">FAQ</h5>
          <ul className="footer-list list-unstyled text-center">
            <li>
              <a href="#!" className="links hover-link">Shipping & Returns</a>
            </li>
            <li>
              <a href="#!" className="links hover-link">Contact Us</a>
            </li>
          </ul>
        </div>
      </div>
    </div>

    <div className="footer-copyright text-center py-3">
      ©2023 BOOKS.com
    </div>
  </footer>
);

export default Footer;