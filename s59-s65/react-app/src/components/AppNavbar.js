import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Container, NavbarBrand, Nav, NavItem, NavLink, Collapse } from 'react-bootstrap';
import UserContext, { UserProvider } from '../UserContext';
import ShoppingBagOutlinedIcon from '@mui/icons-material/ShoppingBagOutlined';
import PersonOutlineOutlinedIcon from '@mui/icons-material/PersonOutlineOutlined';
import LogoutIcon from '@mui/icons-material/Logout';

export default function AppNavbar() {
  const { user, logout } = useContext(UserContext); // Updated to include the logout function
  const [showNavNoTogglerSecond, setShowNavNoTogglerSecond] = useState(false);
  const [iconsActive, setIconsActive] = useState('pill1');

  const handleIconsClick = (value) => {
    if (value === iconsActive) {
      return;
    }
    setIconsActive(value);
  };

  return (
    <UserProvider value={{ user, logout }}> {/* Updated to include the logout function */}
      <Navbar className="navbar sticky-top" expand="lg">
        <Container fluid={true}>
          <NavbarBrand href="#">
            <img
              src={process.env.PUBLIC_URL + '/images/footer-logo.png'}
              alt="Logo"
              className="img-fluid logo"
            />
          </NavbarBrand>

          <Navbar.Toggle
            aria-controls="navbarTogglerDemo02"
            aria-expanded="false"
            aria-label="Toggle navigation"
            onClick={() => setShowNavNoTogglerSecond(!showNavNoTogglerSecond)}
          >
            <span className="navbar-toggler-icon" />
          </Navbar.Toggle>

          <Navbar.Collapse id="navbarTogglerDemo02">
            <Nav className="navbar-nav mx-auto">
              <NavItem>
                <NavLink
                  onClick={() => handleIconsClick('pill1')}
                  active={iconsActive === 'pill1'}
                >
                  SHOP
                </NavLink>
              </NavItem>

              <NavItem className="px-lg-5">
                <NavLink
                  onClick={() => handleIconsClick('pill2')}
                  active={iconsActive === 'pill2'}
                >
                  ABOUT US
                </NavLink>
              </NavItem>

              <NavItem>
                <NavLink
                  onClick={() => handleIconsClick('pill3')}
                  active={iconsActive === 'pill3'}
                >
                  CONTACT
                </NavLink>
              </NavItem>
            </Nav>
            <Nav>
              <NavItem className="py-md-2 mr-lg-5">
                <Link to="/shopping-cart">
                  <ShoppingBagOutlinedIcon className="MuiSvgIcon-root" />
                  <span className="d-inline d-lg-none">Cart</span>
                </Link>
              </NavItem>
              <NavItem className="py-md-2">
                {user.id !== null ? (
                  <Link to="/logout">
                    <LogoutIcon className="MuiSvgIcon-root" />
                    <span className="d-inline d-lg-none">Logout</span>
                  </Link>
                ) : (
                  <Link to="/login">
                    <PersonOutlineOutlinedIcon className="MuiSvgIcon-root" />
                    <span className="d-inline d-lg-none">Login</span>
                  </Link>
                )}
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </UserProvider>
  );
}