import React from 'react';
import Container from 'react-bootstrap/Container';

export default function Main() {
  return (
    <Container fluid="true">
      <div className="my-3 my-md-5 py-3 py-md-5">
        <div className="my-3 my-md-5 py-3 py-md-5">
          <div className="d-flex justify-content-center mt-4 mt-lg-5 pt-lg-5">
            <img
              src={process.env.PUBLIC_URL + '/images/main-text.png'}
              alt='main-text'
              className="main-text img-fluid"
            />
          </div>

          <div className="d-flex justify-content-center mt-2 mt-lg-3">
                  <h3 className="h3-fluid">Escape into the Pages</h3>
                </div>

          <div className="d-flex justify-content-center my-3 my-lg-5">
            <button className="shop-now-btn">Shop Now</button>
          </div>
        </div>
      </div>
    </Container>
  );
}