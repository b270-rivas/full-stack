import React from 'react';
import Container from 'react-bootstrap/Container';

export default function Collection() {
  return (
    <Container fluid="true" className="container-col my-5 pt-5 py-lg-5">
      <div className="collection-text container-fluid p-0">
        <div className="row d-flex justify-content-center my-lg-5">
          <div className="card col-9 col-md-3 mb-md-5 mb-xl-0 p-0 text-center">
            <div className="card-body">
              <span className="col-btn">NEW!</span>
              <img src="Images/notion.jpg" className="card-img-top" alt="notion" />
            </div>
          </div>

          <div className="card col-9 col-md-3 mb-md-5 mb-xl-0 mx-0 mx-md-5 p-0 text-center">
            <div className="card-body">
              <span className="col-btn">BESTSELLERS</span>
              <img src={process.env.PUBLIC_URL + '/images/bestsellers.gif'} className="card-img-top" alt="gdrive" />
            </div>
          </div>

          <div className="card col-9 col-md-3 mb-5 mb-xl-0 p-0 text-center">
            <div className="card-body">
              <span className="col-btn">SALE</span>
              <img src="Images/fcc.png" className="card-img-top" alt="freecodecamp"/>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
}