import React from 'react';
import Container from 'react-bootstrap/Container';
import { MDBIcon, MDBInput } from 'mdb-react-ui-kit';
import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';
import CardMembershipOutlinedIcon from '@mui/icons-material/CardMembershipOutlined';
import MoneyOutlinedIcon from '@mui/icons-material/MoneyOutlined';

export default function Subfooter() {
  return (
    <Container fluid="true">
      <div className="subfooter text-center my-5 py-4 py-md-5 px-3 px-lg-0">
        {/* ROW 1 */}
        <div className="row p">
          {/* Column 1 */}
          <div className="col-12 col-md pr-md-0">
            <LocalShippingOutlinedIcon />
            <h5 className="subfooter-h5">FREE SHIPPING</h5>
            <p className="subfooter-text">Shop Php 799 and above to get your order delivered for free!</p>
          </div>
          {/* Column 2 */}
          <div className="col-12 col-md p-md-0">
            <CardMembershipOutlinedIcon />
            <h5 className="subfooter-h5">MEMBERSHIP DISCOUNT</h5>
            <p className="subfooter-text">Cardholders enjoy an additional 5% off on D-Coded items.</p>
          </div>
          {/* Column 3 */}
          <div className="col-12 col-md pl-md-0">
            <MoneyOutlinedIcon />
            <h5 className="subfooter-h5">CASH ON DELIVERY</h5>
            <p className="subfooter-text">Cash on Delivery available for orders worth Php 799 and above.</p>
          </div>
        </div>
        {/* HORIZONTAL LINE */}
        <hr />
        {/* ROW 2 */}
        <div className="row d-flex justify-content-center">
          {/* Newsletter Subscription */}
          <div className="subfooter-h1 col-12 col-md-5">
            <h1>Subscribe to our newsletter</h1> {/* Updated to h1 */}
          </div>
          {/* Input Field */}
          <div className="col-12 col-md-5 my-auto">
            <MDBInput
              placeholder="Email"
              icon={<MDBIcon icon="envelope" />}
              type="email"
            />
          </div>
        </div>
      </div>
    </Container>
  );
}
