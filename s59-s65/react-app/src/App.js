import './App.css';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Register from './pages/Register';
import { UserProvider } from './UserContext';


export default function App() {

    // Global state hook for the user information for validating if a user is logged in
    const [user, setUser] = useState({
        id: null,
        isAdmin: null
    });

    // Function for clearing localStorage on logout
    const unsetUser = () => {
        localStorage.clear();
    }

    // Used to check if the user info is properly stored upon login and if the localStorage is cleared upon logout
    useEffect(() => {
        console.log(user);
        console.log(localStorage);
    }, [user])

    useEffect(() => {
        fetch("https://e-commerce-api-eru4.onrender.com/user/details", {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {

            if(data._id !== undefined) {

            // Sets the use state values with the user details upon successful login
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });

            // Sets the user state to the initial value 
            } else {
                setUser({
                    id: null,
                    isAdmin: null
                });
            }

        });
    }, []);

    return (
        <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
            <Container fluid>
                <Routes>
                  <Route path="/" element={<Home />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="/login" element={<Login />} />
                  <Route path="/logout" element={<Logout />} />
                  <Route path="*" element={<Error />} />
                </Routes>
             </Container>
        </Router>
     </UserProvider>
    );
}