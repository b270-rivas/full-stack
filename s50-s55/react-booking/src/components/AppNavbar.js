import Container from 'react-bootstrap/Container';
import { Fragment, useContext } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar(){

	// State to store the user information stored in the login page.
	/*const [user, setUser] = useState(localStorage.getItem("email"));
	console.log(user);*/

	const { user } = useContext(UserContext);

	return (

		<Navbar bg="light" expand="lg">
		    <Container fluid>
		        <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          	<Nav className="me-auto">
			            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
			            <Nav.Link as={NavLink} to="/courses">Courses</Nav.Link>
			            {(user.id !== null)?
			            	<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
			            	:
			            	<Fragment>
			            		<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
			            		<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
			            	</Fragment>
			            }
		          	</Nav>
		        </Navbar.Collapse>
		     </Container>
		</Navbar>
	)
}