// import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

    console.log(courseProp);

    // Deconstructs the course properties into their own variables
    const {_id, name, description, price } = courseProp;

    // Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element

    // Syntax: const [getter, setter] = useState(initialGetterValue)
    // const [ count, setCount ] = useState(0);

    // Use state hook for getting and setting the seats for this course
    // const [seats, setSeats] = useState(30);
    // const [isOpen, setIsOpen] = useState(false);

    // Function that keeps track of the enrollees for a course
    // By default JavaScript is synchronous, so it executes code from the top of the file all the way to the bottom and will wait for the completion of one expression before it proceeds to the next
    // The setter function for useStates is asynchronous allowing it to execute separately from other codes in the program
    // The "setCount" function is being executed while the "console.log" is already completed resulting in the value to be displayed in the console to be behind by one count
    // function enroll(){
 //        setCount(count + 1);
 //        console.log('Enrollees: ' + count);
 //        setSeats(seats - 1);
 //        console.log('Seats: ' + seats);
 //    }

    // Define a "useEffect" hook to have the "CourseCard" component do perform a certain task after every DOM update
    // This is run automatically both after initial render and for every DOM update
    // React will re-run this effect ONLY if any of the values contained in the seats array has changed from the last render / update

    // The useEffect hook accepts two arguments: a callback function that represents the effect, and an optional array of dependencies that determine when the effect should be re-evaluated
    // useEffect(() => {
    //     if(seats === 0) {
    //         setIsOpen(true);
    //     }
    // }, [seats]);

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {price}</Card.Text>
                <Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
            </Card.Body>
        </Card>
    )
}