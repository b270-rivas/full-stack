import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// Import Bootstrap
import 'bootstrap/dist/css/bootstrap.css';

//createRoot - assigns the element to be managed by React with its Virtual DOM
const root = ReactDOM.createRoot(document.getElementById('root'));

// render() - displays the react elements/components into the root
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// const name = "John Smith";
// const user = {
//   firstName: "Jane",
//   lastName: "Smith"
// }

// function formatName(user) {
//   return user.firstName + " " + user.lastName;
// }

// const element = <h1>Hello, {formatName(user)}</h1>

// const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(element)