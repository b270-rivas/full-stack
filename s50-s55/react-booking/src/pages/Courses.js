import { Fragment, useEffect, useState } from 'react';
// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses() {
	// Checks to see if the mock data was captured
	// console.log(coursesData);

	// State that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	// "map" method loops through the individual course in our mock database and returns a CourseCard component for each course
	// Multiple components created through the map method must have a unique key that will help React JS identify which components/elements have been changed, added or removed
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our coursesData array using the courseProp
	// const courses = coursesData.map(course => {
	// 	return (
	// 		<CourseCard key={course.id} courseProp={course}/>
	// 	)
	// })

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return(
					<CourseCard key={course.id} courseProp={course}/>
				)
			}))
		})

	}, [])

	return (
		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>
	)
}