import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {
    const navigate = useNavigate();

    // To be able to obtain user ID so we can enroll a user
    const { user } = useContext(UserContext);

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [mobileNoError, setMobileNoError] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    // Enable or disable Submit button
    const [isActive, setIsActive] = useState(false);

    // Validation to enable submit button when all conditions are satisfied
    useEffect(() => {
        setIsActive(
            firstName !== '' &&
            lastName !== '' &&
            mobileNo.length >= 11 &&
            email !== '' &&
            password1 !== '' &&
            password1 === password2
            );

        if (mobileNo.length > 0 && mobileNo.length < 11) {
            setMobileNoError('Mobile number should be at least 11 digits');
        } else {
            setMobileNoError('');
        }

    }, [firstName, lastName, mobileNo, email, password1, password2]);

    // Function to simulate user registration
    function registerUser(e) {
        
        // Prevents page redirection via form submission
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Kindly provide another email to complete the registration.'  
                });

            } else {

                fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo: mobileNo,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if (data) {

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            text: 'Welcome to Zuitt!'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        navigate("/login");

                    } else {

                        Swal.fire({
                            title: 'Something went wrong.',
                            icon: 'error',
                            text: 'Please try again.'   
                        });

                         
                        
                    }
                })
            }
        })
    }

    return (
        user.id !== null ? (
            <Navigate to="/courses" />
            ) : (
            <Form onSubmit={registerUser}>
            <h1 className="mb-3">Register</h1>

            <Form.Group className="mb-3" controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control
                type="text"
                placeholder="First Name"
                required
                value={firstName}
                onChange={e => setFirstName(e.target.value)}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control
                type="text"
                placeholder="Last Name"
                required
                value={lastName}
                onChange={e => setLastName(e.target.value)}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control
                type="number"
                placeholder="Mobile Number"
                required
                value={mobileNo}
                onChange={e => setMobileNo(e.target.value)}
                className={mobileNoError ? 'error' : ''}
                />
                {mobileNoError && (
                    <p className="error-text">{mobileNoError}</p>
                    )}
            </Form.Group>

            <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email address</Form.Label>
                <Form.Control
                type="email"
                placeholder="Enter email"
                required
                value={email}
                onChange={e => setEmail(e.target.value)}
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group className="mb-3" controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control
                type="password"
                placeholder="Password"
                required
                value={password1}
                onChange={e => setPassword1(e.target.value)}
                />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control
                type="password"
                placeholder="Verify Password"
                required
                value={password2}
                onChange={e => setPassword2(e.target.value)}
                />
            </Form.Group>

            {isActive ? (
                <Button className="mb-3" variant="primary" type="submit">
                    Submit
                </Button>
                ) : (
                <Button className="mb-3" variant="danger" type="submit" disabled>
                    Submit
                </Button>
                )
            }
            </Form>
            )
    );
}