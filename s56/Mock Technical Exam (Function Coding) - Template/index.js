function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if (letter.length === 1) {
        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        for (let i = 0; i < sentence.length; i++) {
            // Check if the current character matches the letter
            if (sentence[i] === letter) {
                result++;
            }
        }
        return result;
    } else {
        return undefined; // If letter is invalid, return undefined.
    }
    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    
    // The function should disregard text casing before doing anything else.
    const lowercaseText = text.toLowerCase();

    // Create an empty object to keep track of encountered letters
    const letterCount = {};

    // Loop through each character in the lowercase text
    for (let i = 0; i < lowercaseText.length; i++) {
        const letter = lowercaseText[i];

        // If the function finds a repeating letter, return false.
        if (letterCount[letter]) {
            return false;
        }

        // Otherwise, add the letter to the letterCount object
        letterCount[letter] = true;
    }
    // Otherwise, return true.
    return true;   
};

function purchase(age, price) {
    
    // Return undefined for people aged below 13.
    if (age < 13) {
        return undefined;
    } else if (age >= 13 && age <= 21 || age >= 65) {
        // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
        const discountedPrice = price * 0.8;
        const roundedPrice = discountedPrice.toFixed(2);
        return roundedPrice.toString(); // The returned value should be a string.
    } else if (age >= 22 && age <= 64) {
        // Return the rounded off price for people aged 22 to 64.
        const roundedPrice = price.toFixed(2);
        return roundedPrice.toString(); // The returned value should be a string.
    }
};

function findHotCategories(items) {

    const hotCategories = [];

    for (let i = 0; i < items.length; i++) {
        const item = items[i];

        // Find categories that has no more stocks.
        // The hot categories must be unique; no repeating categories.
        if (item.stocks === 0 && !hotCategories.includes(item.category)) {
            
            hotCategories.push(item.category);
        }
    }
    return hotCategories;
};

function findFlyingVoters(candidateA, candidateB) {

    // Find voters who voted for both candidate A and candidate B.
    let flyingVoters = [];

    // Loop through candidateA array
    for (let i = 0; i < candidateA.length; i++) {
        const flyingVotersA = candidateA[i];
      
        // Loop through candidateB array
        for (let j = 0; j < candidateB.length; j++) {
            const flyingVotersB = candidateB[j];
        
            // Check if the current element in candidateA is equal to the current element in candidateB
            if (flyingVotersA === flyingVotersB) {
            // If they are equal, push the common element to the commonElements array
            flyingVoters.push(flyingVotersA);
            break;
            }
        }
    }
    return flyingVoters;
};

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};