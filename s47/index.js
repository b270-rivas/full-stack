
/*Syntax:
	"document" - refers to thw whole page
	".querySelector" - used to select a specific object/HTML element from the document.
*/
const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

// An event is any interaction initiated by a user within a webpage.
// Event Listeners - used to detect and handle user interactions with a webpage.
/*
	"addEventListener" takes 2 arguments:
		1. A string identifying an even.
		2. A function that the listener will execute once the even is triggered.
*/

/*txtFirstName.addEventListener('keyup', (event) => {

	// The "innerHTML" propery sets or returns the value of the element.
	// The ".value" property sets or returns the value of an attribute. Form control elements: input, select, etc.
	spanFullName.innerHTML = txtFirstName.value;
});*/

txtFirstName.addEventListener('keyup', updateFullName);
txtLastName.addEventListener('keyup', updateFullName);

function updateFullName() {
  const fullName = txtFirstName.value + " " + txtLastName.value;
  spanFullName.innerHTML = fullName;
}

txtFirstName.addEventListener = ('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value);
})