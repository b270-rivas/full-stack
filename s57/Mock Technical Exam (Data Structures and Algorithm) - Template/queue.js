let collection = [];

// Write the queue functions below:

module.exports = {

	print: function() {
		return collection;
	},

	//[2 & 3] Enqueue a new element.
	enqueue: function(element) {
		collection[collection.length] = element;
		return collection;
	},


	//[4] Dequeue the first element.
	dequeue: function() {

		if (collection.length === 0) { // Check if the queue is empty
		return ({error: "Queue is empty"}); // Return error if the queue is empty
		}

		let firstElement = collection[collection.length - 1]; // Store the first element of the queue

		for (let i = 0; i < collection.length - 1; i++) { // Loop through the collection array
		collection[i] = collection[i + 1]; // Shift each element one position to the left
		}

		collection.length--; // Decrease the length of the collection array by 1

		let newfirstElement = []; // Create a new array to store the dequeued element
		newfirstElement[0] = collection[0]; // Get the first element of the queue

		return newfirstElement; // Return the new array containing the dequeued element
	},

	front: function() {

		if (collection.length === 0) { // Check if the queue is empty
		return ({error: "Queue is empty"}); // Return error if the queue is empty
		}

		return collection[0];
	},

	size: function() {
		return collection.length;
	},

	isEmpty: function() {
		return collection.length === 0;
	}

};
